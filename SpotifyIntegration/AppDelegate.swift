//
//  AppDelegate.swift
//  SpotifyIntegration
//
//  Copyright © 2019 john. All rights reserved.
//

import Cocoa
import OAuthSwift

@NSApplicationMain

class AppDelegate: NSObject, NSApplicationDelegate{

    let defaults = UserDefaults.init(suiteName: "com.fusionblender.spotify")
   
    func applicationWillFinishLaunching(_ notification: Notification) {
        
        let window = NSApplication.shared.mainWindow!
        window.makeKeyAndOrderFront(self)
        
        if let appFrame = defaults?.string(forKey: Settings.Keys.windowPosition){
            
            let frame:NSRect = NSRectFromString(appFrame)
            window.setFrame(frame, display: false)
            
        }
        else{
            window.center()
        }
        
        if let fullScreenEnabled = defaults?.bool(forKey: Settings.Keys.appFullScreen){
            if fullScreenEnabled{
                window.toggleFullScreen(self)
            }
        }
        
    }
   
    
    func applicationDidFinishLaunching(_ notification: Notification) {
    
        NSAppleEventManager.shared().setEventHandler(self, andSelector:#selector(AppDelegate.handleGetURL(event:withReplyEvent:)), forEventClass: AEEventClass(kInternetEventClass), andEventID: AEEventID(kAEGetURL))
        
    }

    func applicationDidResignActive(_ notification: Notification) {
        // Might put code to save location here
    }
    
    
    func applicationWillTerminate(_ notification: Notification) {
        let window = NSApplication.shared.mainWindow!
        print("Application will terminate \(window.frame)")
        defaults?.set(NSStringFromRect(window.frame), forKey: Settings.Keys.windowPosition)
        defaults?.synchronize()
    }
    
    @objc func handleGetURL(event: NSAppleEventDescriptor!, withReplyEvent: NSAppleEventDescriptor!){
        if let urlString = event.paramDescriptor(forKeyword: AEKeyword(keyDirectObject))?.stringValue, let url = URL(string: urlString) {
            print("handleGetURL \(url)")
          
           
        }
    }
    
   

    
}

