//
//  SpotifyLoginProtocol.swift
//  SpotifyIntegration
//
//  Copyright © 2019 john. All rights reserved.
//

import Foundation
import OAuthSwift

protocol SpotifyLoginProtocol{
    
    func loginSuccess(code:String, state:String)
    func loginFailure(msg:String)
    
}
