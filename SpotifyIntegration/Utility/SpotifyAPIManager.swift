//
//  SpotifyAPIManager.swift
//  SpotifyIntegration
//
//  Copyright © 2019 john. All rights reserved.
//

import Foundation
import Alamofire
import OAuthSwift

class SpotifyAPIManager{
    
    var baseURL:String = "https://api.spotify.com"
    var authURLString =  "https://accounts.spotify.com/authorize"
    var oauth2: OAuth2Swift!
    
    var accessToken:String!
    var refreshToken:String!
    var scopes: [String]!
    var expires:Int!
    
    static let shared = SpotifyAPIManager()
    
    private init(){
        
        oauth2 = OAuth2Swift(
            consumerKey:    "ENTER YOUR PUBLIC SPOTIFY KEY HERE",
            consumerSecret: "ENTER YOUR SECRET SPOTIFY KEY HERE",
            authorizeUrl:   "https://accounts.spotify.com/en/authorize",
            accessTokenUrl: "https://accounts.spotify.com/api/token",
            responseType:   "code"
        )
        
        
    }
    
    /**
      Initial call used to send Spotify app credentials and scope request.
     
     The Spotify service will either respond with a URI that contains a code to exchange
     for an access token or a login URI if the user is not logged in.
     
     - Parameters: None
     
     - Returns: Void
     */
    func authorizeScope(){

        oauth2.authorize(
            withCallbackURL: URL(string: "http://localhost:8080")!,
            scope: "user-library-modify playlist-read-collaborative playlist-read-private playlist-modify-private playlist-modify-public user-read-currently-playing user-modify-playback-state user-read-playback-state user-library-modify user-library-read user-follow-modify user-follow-read user-read-recently-played user-top-read  user-read-private",
            state: "test12345") { result in
                switch result {
                case .success(let (credential, _, _)):
                    print("Authorization Success")
                case .failure(let error):
                    print(error.description)
                }
        }

    }
    
    
    /**
     Request Spotify API for access and refresh tokens using unique code
     If successful, the Spotify service will respond with the access token, refresh token,
     expiry information of the token, and the available scopes.
     
     - Parameters:
        - code: unique string that is exchanged for access and refresh tokens
        - completion: closure that passes back the OAuthSwift success/failure result to the caller
     
     - Returns: Void
     */
    func authorizeWithRequestToken(code:String, completion: @escaping (Result<OAuthSwift.TokenSuccess, OAuthSwiftError>) -> ()) {
        
        oauth2.postOAuthAccessTokenWithRequestToken(byCode: code, callbackURL: URL.init(string: "http://localhost:8080")!) { result in
            
            switch result{
                
            case .failure(let error):
                print("postOAuthAccessTokenWithRequestToken Error: \(error)")
                completion(result)
                
            case .success(let response):
                
                print("Received Authorization Token: ")
                print(response)
                
                if let access_token = response.parameters["access_token"], let refresh_token = response.parameters["refresh_token"], let expires = response.parameters["expires_in"], let scope = response.parameters["scope"]{
                    
                    
                    
                    self.refreshToken = refresh_token as? String
                    self.accessToken = access_token as? String
                    
                    if let t_scope = scope as? String{
                        let t_vals = t_scope.split(separator:" ")
                        self.scopes = [String]()
                        t_vals.forEach({ scopeParameter in
                            self.scopes.append(String(scopeParameter))
                        })
                    }
                    
                    self.expires = expires as? Int
                    
                    print("ACCESS TOKEN \(String(describing: self.accessToken))")
                    print("REFRESH TOKEN \(String(describing: self.refreshToken))")
                    print("EXPIRES \(String(describing: self.expires))")
                    print("SCOPE: \(String(describing: self.scopes))")
                    
                    completion(result)
                }
            }

        }
        
        
    }
    
    
    
    /**
     Request user information from the Spotify service
     
     - Parameters: None
     
     - Returns: Currently, Void.  Will parse JSON and return usable data structure
     */
    func getSpotifyAccountInfo(completed: @escaping (AFDataResponse<Any>)->()){
        
        let aboutURL = baseURL + "/v1/me"
        let headers: HTTPHeaders = [
            "Authorization": "Bearer " + self.accessToken,
        ]
        
        
        AF.request(aboutURL,
                   headers: headers).responseJSON { response in
                    completed(response)
        }
        
    }
    
    
    
    /**
     Request user information from the Spotify service
     
     - Parameters: None
     
     - Returns: Currently, Void.  Will parse JSON and return usable data structure
     */
    func setAuthorizeHandler(vc:OAuthSwiftURLHandlerType){
         oauth2.authorizeURLHandler = vc
    }
    
    /**
     Set the refresh and access tokens of the manager remotely.
     
     - Parameters:
        - refresh: valid refresh token string from Spotify service
        - access: valid access token string from Spotify service.
     - Returns:  Void
     */
    func setTokens(refresh:String, access:String){
        self.refreshToken = refresh
        self.accessToken = access
    }
    
    /**
     Set the user authorized scopes
     
     - Parameters:
        - scopes: array of valid Spotify scope strings
     
     - Returns: Void
     */
    func setScopes(scopes:[String]){
        
        self.scopes = scopes
        
    }
    
    /**
     Sets the current value in seconds of when the access token expires
     
     - Parameters:
        - expires: integer value from Spotify service in seconds when token expires
     
     - Returns: Void
     */
    func setExpires(expires:Int){
        
        self.expires = expires
    }
    
   
}
