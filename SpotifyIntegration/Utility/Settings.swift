//
//  Settings.swift
//  SpotifyIntegration
//
//  Copyright © 2019 john. All rights reserved.
//

import Foundation

struct Settings{
    
    struct Keys{
        static var windowPosition:String = "AppScreenSizeAndPosition"
        static var appFullScreen:String = "appFullScreen"
    }
}
