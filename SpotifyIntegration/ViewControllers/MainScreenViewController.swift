//
//  ViewController.swift
//  SpotifyIntegration
//
//  Copyright © 2019 john. All rights reserved.
//

import Cocoa
import OAuthSwift
import WebKit


class MainScreenViewController: NSViewController {

    let defaults = UserDefaults.init(suiteName: "com.fusionblender.spotify")
    var webView: WKWebView!
    var spotifyManager:SpotifyAPIManager = SpotifyAPIManager.shared

    @IBOutlet weak var profileImageView: NSImageView!
    
    @IBOutlet weak var profileNameTextField: NSTextField!
    @IBOutlet weak var spotifyAccountTypeTextField: NSTextField!
    @IBOutlet weak var spotifyPublicProfileTextField: NSTextField!
    @IBOutlet weak var spotifyFollowersTextField: NSTextField!
    
    //MARK:- Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()

        let webConfiguration = WKWebViewConfiguration()
        webView = WKWebView(frame: .zero, configuration: webConfiguration)
        webView.uiDelegate = self
        webView.navigationDelegate = self
        
        
        webView.cleanAllCookies()
        
    }

    
    override func viewDidAppear() {
        let window = NSApplication.shared.mainWindow!
        window.delegate = self
        
        spotifyManager.setAuthorizeHandler(vc: self)
        spotifyManager.authorizeScope()
    }

   
    override func prepare(for segue: NSStoryboardSegue, sender: Any?) {
       
        if let swlvc = segue.destinationController as? SpotifyWebLoginViewController{
            swlvc.loginURL = webView.url
            swlvc.loginDelegate = self
        }
    }
    
    
    /**
     Updates the main screen labels and NSImageView with data from Spotify.
     Called after querying Spotify using getSpotifyAccountInfo
     
     - Parameters:
        - json: the JSON dictionary containing the response values for the user data
     
     - Returns: Void
     */
    private func updateUserProfileScreen(json:NSDictionary){
        
        profileNameTextField.stringValue = json["display_name"] as! String
        spotifyAccountTypeTextField.stringValue = json["product"] as! String + "account"
        
        let external_urls = json["external_urls"] as! NSDictionary
        spotifyPublicProfileTextField.stringValue = external_urls["spotify"] as! String
        
        let followersDict = json["followers"] as! NSDictionary
        let followers = followersDict["total"] as! Int64
        let followersText = followers == 1 ? "follower" : "followers"
        spotifyFollowersTextField.stringValue = String(followers) + " " + followersText
        
        let profilePathArr = json["images"] as! NSArray
        let profilePathDict = profilePathArr[0] as! NSDictionary
        
        DispatchQueue.main.async{
            
            do{
                
                let data = try Data(contentsOf: URL(string: profilePathDict["url"] as! String)!)
                var image: NSImage?
                image = NSImage.init(data: data)
                
                guard let t_img = image else{
                    self.profileImageView.image = NSImage.init(named: "profile")
                    return
                }
                
                if t_img.isValid{
                    self.profileImageView.image = t_img
                    self.profileImageView.makeRounded()
                }

            } catch {
                print("Error downloading profile image")
            }
        }

    }
    
}


//MARK:- WKUIDelegate
extension MainScreenViewController:WKUIDelegate{
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        if keyPath == "estimatedProgress" {
            print(Float(webView.estimatedProgress))
        }
    }
}


//MARK:- OAuthSwiftURLHandler
extension MainScreenViewController: OAuthSwiftURLHandlerType {
    
    /**
     Handler for OAuthSwift.
     
     Handler function callback when OAuthSwift instance makes initial request for authorization.
     Permits use of webview to negotiate redirects from Spotify and intercept navigation with
     webview methods.
     
     - Parameters:
     - url: the full url from OAuthSwift initial request for authorization
     
     - Returns: Void
     */
    func handle(_ url: URL) {

        let request = URLRequest(url: url)
        self.webView.load(request)
       
    }

}

//MARK:- WKNavigationDelegate
extension MainScreenViewController: WKNavigationDelegate{
    
    /**
     Called when a WebView receives a redirect.
     
     If Spotify user isn't logged in then redirect flow will check the incoming URL, invoke the
     SpotifyWebLoginVC to login, and pass back the access token via the SpotifyLoginProtocol elegate.
     If user is logged in already, authorization component is invoked. Called after oauth2.AUTHORIZE
     returns a redirect to localhost:8080. Gets the temporary request code and uses it in a call to
     oauth2.postOAuthAccessTokenWithRequestCode that returns the temporary access token used to call Spotify APIs.
     
     - Parameters:
        - webView: the invoking webView
        - navigation:contains information for tracking the loading progress of a webpage.

     - Returns: Void
     */
    
    public func webView(_ webView: WKWebView, didReceiveServerRedirectForProvisionalNavigation navigation: WKNavigation!) {
        
        var code:String? = nil
        var state:String? = nil
        
        if let t_url = webView.url{
            
            // If redirect is to Login then user isn't logged in according to initial OAuth2 request.
            // Segue to SpotifyWebLoginViewController for login flow
            if t_url.lastPathComponent == "login"{
                self.performSegue(withIdentifier: "segueWebLogin", sender: self)
                
            }else{
                
                //Redirect after initial OAuth2 request for authorization.
                //Contains code to pass back to Spotify for Access and Refresh tokens
                if let queryItems = NSURLComponents(string: t_url.description)?.queryItems {
                    
                    for item in queryItems {
                        if item.name == "code" {
                            if let itemValue = item.value {
                                code = itemValue
                            }
                        }else if item.name == "state"{
                            if let itemValue = item.value{
                                state = itemValue
                            }
                        }
                        
                        
                    }
                    
                    if let code_found = code{
                        
                        // Get Access and Refresh tokens from Spotify
                        self.spotifyManager.authorizeWithRequestToken(code: code_found, completion:{ response in
                            
                            switch response{
                                case .success(let (credential, _, _)):
                                    print("Authorization Success")
                                    self.spotifyManager.getSpotifyAccountInfo(completed: { response in
                                        
                                        switch response.result {
                                        case .success:
                                            
                                            
                                            let JSON = response.value as! NSDictionary
                                            print(JSON)
                                            self.updateUserProfileScreen(json: JSON)
                                            
                                        case let .failure(error):
                                            print(error)
                                        }
                                        
                                    })
                                case .failure(let error):
                                    print(error.description)
                            }

                        })
                        
                    }

                }
                
            }
        }
        
    }
    
    /**
     Decides whether to allow or cancel navigation
     Invoke the segue if user is logged in and needs to reauthorize Spotify Scope
     
     - Parameter webView: the webview that invokes the navigation action
     
     - Returns: Void
     */
    public func webView(_ webView: WKWebView, decidePolicyFor navigationAction: WKNavigationAction, decisionHandler: @escaping (WKNavigationActionPolicy) -> Swift.Void){
        
        print("MainScreenVC: Deciding Policy")
        
        if(navigationAction.navigationType == .other)
        {
            if navigationAction.request.url != nil
            {

                // Initial or access token request
                if navigationAction.request.url?.lastPathComponent == "authorize" || navigationAction.request.url?.host == "localhost"{
                    decisionHandler(.allow)
                    return
                    
                }else{
                
                    self.performSegue(withIdentifier: "segueWebLogin", sender: self)
                    
                }
        
            }
            decisionHandler(.cancel)
            return
        }
        
      
        decisionHandler(.cancel)
    }
    
    
    
    func webView(webView: WKWebView, didFailProvisionalNavigation navigation: WKNavigation!, withError error: NSError) {
        print(error.localizedDescription)
    }
    func webView(webView: WKWebView, didStartProvisionalNavigation navigation: WKNavigation!) {
        print("Starting to load")
    }
    func webView(webView: WKWebView, didFinishNavigation navigation: WKNavigation!) {
        print("Finishing loading")
    }
    
}



//MARK:- NSWindowDelegate
extension MainScreenViewController: NSWindowDelegate{
    
    /*
    func window(_ window: NSWindow, willPositionSheet sheet: NSWindow, using rect: NSRect) -> NSRect {
       return NSRect.init(x:0, y: 0, width: 300, height: 600)
    }
    
    func windowDidEnterFullScreen(_ notification: Notification) {
        defaults?.set(true, forKey: Settings.Keys.appFullScreen)
    }
    
    func windowDidExitFullScreen(_ notification: Notification) {
        defaults?.set(false, forKey: Settings.Keys.appFullScreen)
    }*/
    
    func windowShouldClose(_ sender: NSWindow) -> Bool {
        
        let window = NSApplication.shared.mainWindow!
        if(sender == window) {
            defaults?.set(window.isZoomed ? true : false, forKey:Settings.Keys.appFullScreen)
        }
        return true;
    }
 
 
}


//MARK:- SpotifyLoginProtocol
extension MainScreenViewController:SpotifyLoginProtocol{
    
    /**
     User cancelled authorization from WebLoginViewController
     
     - Parameters
        - msg: request code to send to Spotify for access and refresh tokens
     
     - Returns: Void
     */
    func loginFailure(msg:String) {
        print("Login Failure:" + msg)
    }
    
    
    /**
     User is ogged in from WebLoginViewController. Complete the login process
     
     - Parameters:
        - code: request code to send to Spotify for access and refresh tokens
        - state: used to validate request server-side.  Not used here
     
     - Returns: Void
     */
    func loginSuccess(code:String, state:String) {
        print("Login Success: Code \(code)")
        
        // Complete the authorization, get the access and refresh tokens, call the spotify API
        self.spotifyManager.authorizeWithRequestToken(code: code) { (String) in
            self.spotifyManager.getSpotifyAccountInfo(completed: { response in
                
                switch response.result {
                case .success:
                    
                    let JSON = response.value as! NSDictionary
                    print(JSON)
                    self.updateUserProfileScreen(json: JSON)
                    
                case let .failure(error):
                    print(error)
                }
                
            })

        }
    }
    
}

